import React from "react";
import { 
  View,
  Text,
  StyleSheet
} from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import Router from "./src/router";


const App = () => (
  <NavigationContainer>
      <Router/>
    </NavigationContainer>
  )
export default App;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center'
  }
});