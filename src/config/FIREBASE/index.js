import firebase from "firebase";

firebase.initializeApp({
    apiKey: "AIzaSyDRLsAIjYFv9OPy-j_z1MgLppTkbh7zm-w",
    authDomain: "kontakfirebase.firebaseapp.com",
    databaseURL: "https://kontakfirebase.firebaseio.com",
    projectId: "kontakfirebase",
    storageBucket: "kontakfirebase.appspot.com",
    messagingSenderId: "91508943362",
    appId: "1:91508943362:web:d1b603cde3d640afed951b"
})

const FIREBASE = firebase;
export default FIREBASE;