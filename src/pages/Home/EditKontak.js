import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
    Alert
} from "react-native";
import InputData from "../../components/InputData";
import FIREBASE from '../../config/FIREBASE'

class EditKontak extends Component {
    constructor(props) {
        super(props)

        this.state = {
            nama: '',
            nomor: '',
            alamat: '',
        }
    }

    onChangeText = (nameState, value) => {
        this.setState({
            [nameState]: value
        })
    }

    onSubmit = () => {
        if (this.state.nama && this.state.nomor && this.state.alamat) {
            const kontakReferensi = FIREBASE.database().ref("Kontak/" + this.props.route.params.id);
            const kontak = {
                nama: this.state.nama,
                nomor: this.state.nomor,
                alamat: this.state.alamat
            }

            kontakReferensi
                .update(kontak)
                .then((data) => {
                    Alert.alert("Sukses", "Kontak Terupdate");
                    this.props.navigation.replace('Home')
                })
                .catch((error) => {
                    console.log("Error : ", error)
                })

        } else {
            Alert.alert("Error", "Nama, Nomor HP, dan Alamat wajib diisi")
        }
    }

    componentDidMount() {
        FIREBASE.database()
            .ref("Kontak/" + this.props.route.params.id)
            .once("value", (querySnapShot) => {
                let data = querySnapShot.val() ? querySnapShot.val() : {}
                let kontakItem = { ...data }

                this.setState({
                    nama: kontakItem.nama,
                    nomor: kontakItem.nomor,
                    alamat: kontakItem.alamat
                })
            })
    }

    render() {
        return (
            <View style={styles.container}>
                <InputData
                    label="Nama"
                    placeholder="Masukkan Nama"
                    onChangeText={this.onChangeText}
                    value={this.state.nama}
                    nameState="nama" />

                <InputData
                    label="No. HP"
                    placeholder="Masukkan No. HP"
                    keyboardType="number-pad"
                    onChangeText={this.onChangeText}
                    value={this.state.nomor}
                    nameState="nomor" />

                <InputData
                    label="Alamat"
                    placeholder="Masukkan Alamat"
                    isTextArea={true}
                    onChangeText={this.onChangeText}
                    value={this.state.alamat}
                    nameState="alamat" />

                <TouchableOpacity style={styles.tombol} onPress={() => this.onSubmit()}>
                    <Text style={styles.textTombol}>
                        SUBMIT
                    </Text>
                </TouchableOpacity>
            </View>
        );
    }
}
export default EditKontak;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 30
    },
    tombol: {
        backgroundColor: 'black',
        padding: 10,
        borderRadius: 5,
        marginTop: 10
    },
    textTombol: {
        color: 'white',
        fontWeight: 'bold',
        textAlign: 'center',
        fontSize: 16
    }
});